package julian.marin.juegomillonario.logica;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;


import julian.marin.juegomillonario.R;

public class PreguntasRespuestas {
    private String[] Preguntas;
    private String[] Respuestas;
    private ArrayList<Pregunta>ListaPreguntas = new ArrayList<>();
    private Pregunta pregunta = new Pregunta();
    private List<Pregunta> ListaNivel1;
    private List<Pregunta>ListaNivel2;
    private List<Pregunta>ListaNivel3;
    private List<Pregunta>ListaNivel4;
    private List<Pregunta>ListaNivel5;




    public String[] getRespuestas() {
        return Respuestas;
    }

    public void setRespuestas(String[] respuestas) {
        Respuestas = respuestas;
    }

    public String[] getPreguntas() {
        return Preguntas;
    }

    public void setPreguntas(String[] preguntas) {
        Preguntas = preguntas;
    }

    public ArrayList<Pregunta> getListaPreguntas() {
        return ListaPreguntas;
    }

    public void setListaPreguntas(ArrayList<Pregunta> listaPreguntas) {
        ListaPreguntas = listaPreguntas;
    }

    public Pregunta getPregunta() {
        return pregunta;
    }

    public void setPregunta(Pregunta pregunta) {
        this.pregunta = pregunta;
    }


    public List<Pregunta> getListaNivel1() {
        return ListaNivel1;
    }

    public List<Pregunta> getListaNivel2() {
        return ListaNivel2;
    }

    public void setListaNivel2(ArrayList<Pregunta> listaNivel2) {
        ListaNivel2 = listaNivel2;
    }

    public void setListaNivel1(List<Pregunta> listaNivel1) {
        ListaNivel1 = listaNivel1;
    }

    public void setListaNivel2(List<Pregunta> listaNivel2) {
        ListaNivel2 = listaNivel2;
    }

    public List<Pregunta> getListaNivel3() {
        return ListaNivel3;
    }

    public void setListaNivel3(List<Pregunta> listaNivel3) {
        ListaNivel3 = listaNivel3;
    }

    public List<Pregunta> getListaNivel4() {
        return ListaNivel4;
    }

    public void setListaNivel4(List<Pregunta> listaNivel4) {
        ListaNivel4 = listaNivel4;
    }

    public List<Pregunta> getListaNivel5() {
        return ListaNivel5;
    }

    public void setListaNivel5(List<Pregunta> listaNivel5) {
        ListaNivel5 = listaNivel5;
    }

    public PreguntasRespuestas(Context context) {
        Preguntas = context.getResources().getStringArray( R.array.Preguntas );
        Respuestas = context.getResources().getStringArray( R.array.Respuestas );


        for (int i = 0; i < Preguntas.length; i++) {
            ListaPreguntas.add( new Pregunta( Preguntas[i],Respuestas[i].split( "," ) ));

        }




        ListaNivel1 = ListaPreguntas.subList( 0,4 );
        ListaNivel2 =  ListaPreguntas.subList( 5,9 );
        ListaNivel3 =  ListaPreguntas.subList( 10,14 );
        ListaNivel4 =  ListaPreguntas.subList( 15,19 );
        ListaNivel5 =  ListaPreguntas.subList( 20,24 );

        for(Pregunta p: ListaPreguntas){

            Log.d( "salida", p.getPregunta()+","+p.getRespuestas()[3] );

        }






    }



    }


