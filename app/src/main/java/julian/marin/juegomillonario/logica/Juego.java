package julian.marin.juegomillonario.logica;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Juego {
    Context context;
    private PreguntasRespuestas QyA;

    private TextView preg;
    private TextView ans1;
    private TextView ans2;
    private TextView ans3;
    private TextView ans4;
    private Random random = new Random();
    private boolean cosa = false;
    private  Button reiniciar;
    private int puntaje_1;
    public Juego(Context context, TextView TextPre, TextView TextAns1, TextView TextAns2,
                 TextView TextAns3, TextView TextAns4, Button reinciar) {
        this.context = context;
        this.QyA = new PreguntasRespuestas( this.context );
        this.preg = TextPre;
        this.ans1 = TextAns1;
        this.ans2 = TextAns2;
        this.ans3 = TextAns3;
        this.ans4 = TextAns4;
        this.reiniciar = reinciar;
        reinciar.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast toast = Toast.makeText( context.getApplicationContext(), "Se reinicio", Toast.LENGTH_SHORT);
                toast.show();
                nivel1();
            }
        } );

    }

    public void Jugar() {
        boolean activo = true;
        boolean primera = true;
        boolean segunda = false;
        boolean tercera = false;
        boolean cuarta = false;
        boolean quinta = false;
        boolean perdio = false, sexta = false;

        nivel1();


    }

    public void nivel1() {
        Toast toast = Toast.makeText( context.getApplicationContext(), "Nivel 1", Toast.LENGTH_SHORT);
        toast.show();

        int numPre = random.nextInt( 4 );
        int numres0 = random.nextInt( 3 );
        int numres1 = random.nextInt( 3 );
        int numres2 = random.nextInt( 3 );
        int numres3 = random.nextInt( 3 );
        boolean level1=false;
        Log.d( "TAG", "" + numPre );


        String pregunta_n1;
        String[] respuestas_n1;
        pregunta_n1 = this.QyA.getListaNivel1().get( numPre ).getPregunta();
        respuestas_n1 = this.QyA.getListaNivel1().get( numPre ).getRespuestas();


        List<String> copia = Arrays.asList( respuestas_n1.clone() );
        Collections.shuffle( copia );


        this.preg.setText( pregunta_n1 );
        this.ans1.setText( copia.get( 0 ) );
        this.ans2.setText( copia.get( 1 ) );
        this.ans3.setText( copia.get( 2 ) );
        this.ans4.setText( copia.get( 3 ) );

        this.ans1.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ans1.getText().toString() == respuestas_n1[0]) {
                     boolean level2=true;
                    Log.d( "TAG", "funciona: "+cosa );
                    if(level2){
                        nivel2();
                    }

                } else {
                    Toast toast = Toast.makeText( context.getApplicationContext(), "Perdio", Toast.LENGTH_SHORT);
                    toast.show();
                    nivel1();
                }
            }
        } );


        this.ans2.setOnClickListener( new View.OnClickListener() {
                                          @Override
                                          public void onClick(View view) {
                                              if (ans2.getText().toString() == respuestas_n1[0]) {
                                                  boolean level2=true;
                                                  Log.d( "TAG", "funciona: "+cosa );
                                                  if(level2){
                                                      nivel2();}
                                              } else {
                                                  Toast toast = Toast.makeText( context.getApplicationContext(), "Perdio", Toast.LENGTH_SHORT);
                                                  toast.show();
                                                  nivel1();
                                              }
                                          }
                                      }

        );


        this.ans3.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ans3.getText().toString() == respuestas_n1[0]) {
                    boolean level2=true;
                    Log.d( "TAG", "funciona: "+cosa );
                    if(level2){
                        nivel2();}

                } else {
                    Toast toast = Toast.makeText( context.getApplicationContext(), "Perdio", Toast.LENGTH_SHORT);
                    toast.show();
                    nivel1();
                }
            }
        } );

        this.ans4.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ans4.getText().toString() == respuestas_n1[0]) {
                    boolean level2=true;
                    Log.d( "TAG", "funciona: "+cosa );
                    if(level2){
                        nivel2();}
                } else {
                    Toast toast = Toast.makeText( context.getApplicationContext(), "Perdio", Toast.LENGTH_SHORT);
                    toast.show();
                    nivel1();
                }
            }
        } );


    }


    public void nivel2() {
        Toast toast = Toast.makeText( context.getApplicationContext(), "Nivel2", Toast.LENGTH_SHORT);
        toast.show();
        int numPre = random.nextInt( 4 );
        int numres0 = random.nextInt( 3 );
        int numres1 = random.nextInt( 3 );
        int numres2 = random.nextInt( 3 );
        int numres3 = random.nextInt( 3 );

        Log.d( "TAG", "" + numPre );


        String pregunta_n2;
        String[] respuestas_n2;
        pregunta_n2 = this.QyA.getListaNivel2().get( numPre ).getPregunta();
        respuestas_n2 = this.QyA.getListaNivel2().get( numPre ).getRespuestas();


        List<String> copia2 = Arrays.asList( respuestas_n2.clone() );
        Collections.shuffle( copia2 );


        this.preg.setText( pregunta_n2 );
        this.ans1.setText( copia2.get( 0 ) );
        this.ans2.setText( copia2.get( 1 ) );
        this.ans3.setText( copia2.get( 2 ) );
        this.ans4.setText( copia2.get( 3 ) );

        this.ans1.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ans1.getText().toString() == respuestas_n2[0]) {
                    boolean level3=true;
                    if(level3){
                        nivel3();
                    }else{
                        Toast toast = Toast.makeText( context.getApplicationContext(), "Perdio", Toast.LENGTH_SHORT);
                        toast.show();
                        nivel1();
                    }

                }
            }
        } );


        this.ans2.setOnClickListener( new View.OnClickListener() {
                                          @Override
                                          public void onClick(View view) {
                                              if (ans2.getText().toString() == respuestas_n2[0]) {
                                                  boolean level3=true;
                                                  if(level3){
                                                      nivel3();
                                                  }
                                              }else {
                                                  Toast toast = Toast.makeText( context.getApplicationContext(), "Perdio", Toast.LENGTH_SHORT);
                                                  toast.show();
                                                  nivel1();
                                              }
                                          }
                                      }

        );


        this.ans3.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ans3.getText().toString() == respuestas_n2[0]) {
                    boolean level3=true;
                    if(level3){
                        nivel3();
                    }

                }else{
                    Toast toast = Toast.makeText( context.getApplicationContext(), "Perdio", Toast.LENGTH_SHORT);
                    toast.show();
                    nivel1();
                }
            }
        } );

        this.ans4.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ans4.getText().toString() == respuestas_n2[0]) {
                    boolean level3=true;
                    if(level3){
                        nivel3();
                    }

                }else{
                    Toast toast = Toast.makeText( context.getApplicationContext(), "Perdio", Toast.LENGTH_SHORT);
                    toast.show();
                    nivel1();
                }
            }
        } );


    }


    public void nivel3() {
        Toast toast = Toast.makeText( context.getApplicationContext(), "Nivel3", Toast.LENGTH_SHORT);
        toast.show();
        int numPre = random.nextInt( 4 );
        int numres0 = random.nextInt( 3 );
        int numres1 = random.nextInt( 3 );
        int numres2 = random.nextInt( 3 );
        int numres3 = random.nextInt( 3 );
        boolean level1=false;
        Log.d( "TAG", "" + numPre );


        String pregunta_n1;
        String[] respuestas_n1;
        pregunta_n1 = this.QyA.getListaNivel3().get( numPre ).getPregunta();
        respuestas_n1 = this.QyA.getListaNivel3().get( numPre ).getRespuestas();


        List<String> copia = Arrays.asList( respuestas_n1.clone() );
        Collections.shuffle( copia );


        this.preg.setText( pregunta_n1 );
        this.ans1.setText( copia.get( 0 ) );
        this.ans2.setText( copia.get( 1 ) );
        this.ans3.setText( copia.get( 2 ) );
        this.ans4.setText( copia.get( 3 ) );

        this.ans1.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ans1.getText().toString() == respuestas_n1[0]) {
                    boolean level4=true;
                    Log.d( "TAG", "funciona: "+cosa );
                    if(level4){
                        nivel4();
                    }

                } else {
                    Toast toast = Toast.makeText( context.getApplicationContext(), "Perdio", Toast.LENGTH_SHORT);
                    toast.show();
                    nivel1();
                }
            }
        } );


        this.ans2.setOnClickListener( new View.OnClickListener() {
                                          @Override
                                          public void onClick(View view) {
                                              if (ans2.getText().toString() == respuestas_n1[0]) {
                                                  boolean level4=true;
                                                  Log.d( "TAG", "funciona: "+cosa );
                                                  if(level4){
                                                      nivel4();}
                                              } else {
                                                  Toast toast = Toast.makeText( context.getApplicationContext(), "Perdio", Toast.LENGTH_SHORT);
                                                  toast.show();
                                                  nivel1();
                                              }
                                          }
                                      }

        );


        this.ans3.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ans3.getText().toString() == respuestas_n1[0]) {
                    boolean level4=true;
                    Log.d( "TAG", "funciona: "+cosa );
                    if(level4){
                        nivel4();}

                } else {
                    Toast toast = Toast.makeText( context.getApplicationContext(), "Perdio", Toast.LENGTH_SHORT);
                    toast.show();
                    nivel1();
                }
            }
        } );

        this.ans4.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ans4.getText().toString() == respuestas_n1[0]) {
                    boolean level4=true;
                    Log.d( "TAG", "funciona: "+cosa );
                    if(level4){
                        nivel4();}
                } else {
                    Toast toast = Toast.makeText( context.getApplicationContext(), "Perdio", Toast.LENGTH_SHORT);
                    toast.show();
                    nivel1();
                }
            }
        } );




    }

    public void nivel4() {
        Toast toast = Toast.makeText( context.getApplicationContext(), "Nivel4", Toast.LENGTH_SHORT);
        toast.show();
        int numPre = random.nextInt( 4 );
        int numres0 = random.nextInt( 3 );
        int numres1 = random.nextInt( 3 );
        int numres2 = random.nextInt( 3 );
        int numres3 = random.nextInt( 3 );
        boolean level1=false;
        Log.d( "TAG", "" + numPre );


        String pregunta_n1;
        String[] respuestas_n1;
        pregunta_n1 = this.QyA.getListaNivel4().get( numPre ).getPregunta();
        respuestas_n1 = this.QyA.getListaNivel4().get( numPre ).getRespuestas();


        List<String> copia = Arrays.asList( respuestas_n1.clone() );
        Collections.shuffle( copia );


        this.preg.setText( pregunta_n1 );
        this.ans1.setText( copia.get( 0 ) );
        this.ans2.setText( copia.get( 1 ) );
        this.ans3.setText( copia.get( 2 ) );
        this.ans4.setText( copia.get( 3 ) );

        this.ans1.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ans1.getText().toString() == respuestas_n1[0]) {
                    boolean level5=true;
                    Log.d( "TAG", "funciona: "+cosa );
                    if(level5){
                        nivel5();
                    }

                } else {
                    Toast toast = Toast.makeText( context.getApplicationContext(), "Perdio", Toast.LENGTH_SHORT);
                    toast.show();
                    nivel1();
                }
            }
        } );


        this.ans2.setOnClickListener( new View.OnClickListener() {
                                          @Override
                                          public void onClick(View view) {
                                              if (ans2.getText().toString() == respuestas_n1[0]) {
                                                  boolean level5=true;
                                                  Log.d( "TAG", "funciona: "+cosa );
                                                  if(level5){
                                                      nivel5();}
                                              } else {
                                                  Toast toast = Toast.makeText( context.getApplicationContext(), "Perdio", Toast.LENGTH_SHORT);
                                                  toast.show();
                                                  nivel1();
                                              }
                                          }
                                      }

        );


        this.ans3.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ans3.getText().toString() == respuestas_n1[0]) {
                    boolean level5=true;
                    Log.d( "TAG", "funciona: "+cosa );
                    if(level5){
                        nivel5();}

                } else {
                    Toast toast = Toast.makeText( context.getApplicationContext(), "Perdio", Toast.LENGTH_SHORT);
                    toast.show();
                    nivel1();
                }
            }
        } );

        this.ans4.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ans4.getText().toString() == respuestas_n1[0]) {
                    boolean level5=true;
                    Log.d( "TAG", "funciona: "+cosa );
                    if(level5){
                        nivel5();}
                } else {
                    Toast toast = Toast.makeText( context.getApplicationContext(), "Perdio", Toast.LENGTH_SHORT);
                    toast.show();
                    nivel1();
                }
            }
        } );


    }
    public void nivel5() {
        Toast toast = Toast.makeText( context.getApplicationContext(), "Nivel5", Toast.LENGTH_SHORT);
        toast.show();
        int numPre = random.nextInt( 4 );
        int numres0 = random.nextInt( 3 );
        int numres1 = random.nextInt( 3 );
        int numres2 = random.nextInt( 3 );
        int numres3 = random.nextInt( 3 );
        boolean level1=false;
        Log.d( "TAG", "" + numPre );


        String pregunta_n1;
        String[] respuestas_n1;
        pregunta_n1 = this.QyA.getListaNivel5().get( numPre ).getPregunta();
        respuestas_n1 = this.QyA.getListaNivel5().get( numPre ).getRespuestas();


        List<String> copia = Arrays.asList( respuestas_n1.clone() );
        Collections.shuffle( copia );


        this.preg.setText( pregunta_n1 );
        this.ans1.setText( copia.get( 0 ) );
        this.ans2.setText( copia.get( 1 ) );
        this.ans3.setText( copia.get( 2 ) );
        this.ans4.setText( copia.get( 3 ) );

        this.ans1.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ans1.getText().toString() == respuestas_n1[0]) {
                    boolean level6=true;
                    Log.d( "TAG", "funciona: "+cosa );
                    if(level6){
                        Toast toast = Toast.makeText( context.getApplicationContext(), "Gano", Toast.LENGTH_SHORT);
                        toast.show();;
                    }

                } else {
                    Toast toast = Toast.makeText( context.getApplicationContext(), "Perdio", Toast.LENGTH_SHORT);
                    toast.show();
                    nivel1();     }
            }
        } );


        this.ans2.setOnClickListener( new View.OnClickListener() {
                                          @Override
                                          public void onClick(View view) {
                                              if (ans2.getText().toString() == respuestas_n1[0]) {
                                                  boolean level6=true;
                                                  Log.d( "TAG", "funciona: "+cosa );
                                                  if(level6){
                                                      Toast toast = Toast.makeText( context.getApplicationContext(), "Gano", Toast.LENGTH_SHORT);
                                                      toast.show();}
                                              } else {
                                                  Toast toast = Toast.makeText( context.getApplicationContext(), "Perdio", Toast.LENGTH_SHORT);
                                                  toast.show();
                                                  nivel1();
                                              }
                                          }
                                      }

        );


        this.ans3.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ans3.getText().toString() == respuestas_n1[0]) {
                    boolean level6=true;
                    Log.d( "TAG", "funciona: "+cosa );
                    if(level6){
                        Toast toast = Toast.makeText( context.getApplicationContext(), "Gano", Toast.LENGTH_SHORT);
                        toast.show();}

                } else {
                    Toast toast = Toast.makeText( context.getApplicationContext(), "Perdio", Toast.LENGTH_SHORT);
                    toast.show();
                    nivel1();
                }
            }
        } );

        this.ans4.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ans4.getText().toString() == respuestas_n1[0]) {
                    boolean level6=true;
                    Log.d( "TAG", "funciona: "+cosa );
                    if(level6){
                        Toast toast = Toast.makeText( context.getApplicationContext(), "Gano", Toast.LENGTH_SHORT);
                        toast.show();
                        }
                } else {
                    Toast toast = Toast.makeText( context.getApplicationContext(), "Perdio", Toast.LENGTH_SHORT);
                    toast.show();
                    nivel1();
                }
            }
        } );


    }


public void gano(){

}

}
