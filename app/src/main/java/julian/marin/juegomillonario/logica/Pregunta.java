package julian.marin.juegomillonario.logica;

import java.util.Arrays;

public class Pregunta {

    private     String Pregunta;

    private  String[] Respuestas;

    public Pregunta(String pregunta, String[] respuestas) {
        Pregunta = pregunta;
        Respuestas = respuestas;
    }
    public Pregunta() {

    }

    public String getPregunta() {
        return Pregunta;
    }

    public void setPregunta(String pregunta) {
        Pregunta = pregunta;
    }

    public String[] getRespuestas() {
        return Respuestas;
    }

    public void setRespuestas(String[] respuestas) {
        Respuestas = respuestas;
    }

    @Override
    public String toString() {
        return "Pregunta{" +
                "Pregunta='" + Pregunta + '\'' +
                ", Respuestas=" + Arrays.toString( Respuestas ) +
                '}';
    }
}
